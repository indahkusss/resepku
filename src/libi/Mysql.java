/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author indah
 */
public class Mysql {
    private static Connection con;
    
    public static void cekKoneksi(){
    
        //cek apakah koneksi null
        if(con == null){
            try{
                String url = "jdbc:mysql://localhost/resepkuDB?serverTimezone=UTC";
                String user = "root";
                String pass = "";
                
                DriverManager.registerDriver(new com.mysql.jdbc.Driver());
                
                con = DriverManager.getConnection(url, user, pass);
                System.out.println("berhasil koneksi");
            } catch(SQLException t){
                System.out.println("Errors membuat koneksi");
            }
        }
    }
 
    public static Connection getKoneksi() {
        cekKoneksi();
        return con;
    }
    
    public static ResultSet query(String query) throws SQLException {
        getKoneksi();
        return con.createStatement().executeQuery(query);
    }
}
